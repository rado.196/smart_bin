import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";

import Helper from "../../Helpers";

export default class PeoplesParty extends Component {
    constructor(props, context) {
        super(props, context);

        Helper.pageTitle("Մարդամեջ");
    }

    componentDidMount() {
        Helper.pageTitle("Մարդամեջ");
    }

    componentWillUpdate() {
        Helper.pageTitle("Մարդամեջ");
    }

    render() {
        return (
            <div className="site-section bg-light">
                <Container>
                    <Row>
                        <Col lg={{ size: 10, offset: 1 }}>
                            <div className="p-25 bg-white box-shadowed">
                                <div className="mB-50">
                                    <h1>Մարդամեջ</h1>
                                </div>
                                <div className="mB-25">
                                    <p className="fsz-def">«Մարդամեջ» սոցիալ նորարարության հավաք</p>
                                    <p className="fsz-def">
                                        «Մարդամեջ» ՍՆՀ-ի նպատակն է նպաստել ՀՀ-ում տեղի ունեցող բարեփոխումների (սոցիալական,
                                        տեղական իշխանությունների հզորացման, թափանցիկության և հաշվետվողականության) գործընթացի
                                        մասին տեղեկատվության և հանրային մասնակցության բարձրացմանը, այնպիսի խնդիրների
                                        վերհանմանը, որոնց լուծումը հնարավոր կլինի տալ օնլայն կամ օֆլայն միջոցներով:
                                    </p>
                                    <p className="fsz-def">
                                        Սոցիալ նորարարության հավաքը մեկ տեղ է բերում մարդկանց, գաղափարներ և թվային
                                        տեխնոլոգիաներ՝ առցանց սոցիալական նորարարություններ ստեղծելու համար:
                                    </p>
                                    <p className="fsz-def">
                                        «Մարդամեջ» ՍՆՀ-ն անցկացվում է Ամերիկայի ժողովրդի աջակցությամբ՝ ԱՄՆ Միջազգային
                                        զարգացման գործակալության միջոցով իրականացվող «Մեդիան քաղաքացիների տեղեկացված
                                        մասնակցության համար» և «Հանրային մասնակցություն տեղական ինքնակառավարմանը» ծրագրերի շրջանակներում։
                                    </p>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
