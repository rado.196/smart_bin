import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Row, Col, FormGroup, Label, Input, Button } from "reactstrap";

import * as service from "../../Service/AuthService";
import Route from "../../Routes";

class Login extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            notVerified: false,
            emailOrPhoneData: "",
            emailOrPhoneValid: false,
            passwordData: "",
            passwordValid: false,
        };

        this.validateEmailPhone = this.validateEmailPhone.bind(this);
        this.validatePassword = this.validatePassword.bind(this);
        this.formSubmitLogin = this.formSubmitLogin.bind(this);
    }

    validateEmailPhone(event) {
        let element = event.target;
        if (!service.validEmailOrPhone(element)) {
            this.setState({
                emailOrPhoneData: element.value,
                emailOrPhoneValid: false,
            });
        } else {
            this.setState({
                emailOrPhoneData: element.value,
                emailOrPhoneValid: true,
            });
        }
    }

    validatePassword(event) {
        let element = event.target;
        if (!service.validPassword(element)) {
            this.setState({
                passwordData: element.value,
                passwordValid: false,
            });
        } else {
            this.setState({
                passwordData: element.value,
                passwordValid: true,
            });
        }
    }

    formSubmitLogin(event) {
        event.preventDefault();

        const invalid = !this.state.emailOrPhoneValid ||
            !this.state.passwordValid;

        if (invalid) {
            service.showAllErrors(event.target);
            return;
        }

        service.exec(this, event.target, service.login, {
            email_or_phone: this.state.emailOrPhoneData,
            password: this.state.passwordData,
        });
    }

    render() {
        if (this.props.isLogged) {
            return <Redirect to={Route.toUrl("user_profile")} />
        }

        if (this.state.notVerified) {
            return <Redirect to={Route.toUrl("auth_verify")} />
        }

        return (
            <div className="site-section bg-light">
                <Container>
                    <Row>
                        <Col lg={4} className="offset-lg-4">
                            <form onSubmit={this.formSubmitLogin} className="p-25 bg-white box-shadowed">
                                <h2 className="mb-4 text-black">Մուտք</h2>

                                <FormGroup>
                                    <div className="input-area">
                                        <div className="input-content">
                                            <Label for="email-phone">Էլ-փոստ / Բջջային համար <sup className="input-required fas fa-asterisk" /></Label>
                                            <Input type="text" id="email-phone" className="rounded-0" autoComplete="off" onChange={this.validateEmailPhone} />
                                        </div>
                                        <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                    <div className="input-area">
                                        <div className="input-content">
                                            <Label for="password">Գաղտնաբառ <sup className="input-required fas fa-asterisk" /></Label>
                                            <Input type="password" id="password" className="rounded-0" onChange={this.validatePassword} />
                                        </div>
                                        <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                    <Button color="success" className="w-100 py-2 px-4 rounded-0">Մուտք</Button>
                                    <ul className="auth-error-message" id="auth-error-message" />
                                    <ul className="auth-success-message" id="auth-success-message" />
                                </FormGroup>

                                <div className="loading-area" id="loading-area" />
                                <div className="link-area">
                                    <Link to={Route.toUrl("auth_forgot_password")}>Մոռացել ե՞ք գաղտնաբառը:</Link>
                                </div>
                            </form>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

Login.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isLogged: PropTypes.bool.isRequired,
};

export default connect((state) => {
    return {
        isLogged: state.AuthReducer.isLogged,
    };
})(Login);
