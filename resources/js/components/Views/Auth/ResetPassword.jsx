import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Row, Col, Input, Button, Label, FormGroup } from "reactstrap";

import Route from "../../Routes";
import * as service from "../../Service/AuthService";
import reactTriggerChange from "react-trigger-change";

class ResetPassword extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            resetComplete: false,
            validCode: true,
            passwordData: "",
            passwordValid: false,
            confirmData: "",
            confirmValid: false,
        };

        this.validatePassword = this.validatePassword.bind(this);
        this.validateConfirm = this.validateConfirm.bind(this);
        this.formSubmitResetPassword = this.formSubmitResetPassword.bind(this);
    }

    validatePassword(event) {
        let element = event.target;
        if (!service.validPassword(element)) {
            this.setState({
                passwordData: element.value,
                passwordValid: false
            });
        } else {
            this.setState({
                passwordData: element.value,
                passwordValid: "" != element.value
            });
        }

        const confirm = document.querySelector("input#confirm");
        reactTriggerChange(confirm);
    }

    validateConfirm(event) {
        const password = document.querySelector("input#password");

        let element = event.target;
        if (!service.validConfirm(element, password.value)) {
            this.setState({
                confirmData: element.value,
                confirmValid: false
            });
        } else {
            this.setState({
                confirmData: element.value,
                confirmValid: "" != element.value
            });
        }
    }

    formSubmitResetPassword(event) {
        event.preventDefault();

        const invalid = !this.state.passwordValid ||
            !this.state.confirmValid;

        if (invalid) {
            service.showAllErrors(event.target);
            return;
        }

        service.exec(this, event.target, service.resetPassword, {
            password: this.state.passwordData,
            code: this.props.match.params.code,
        });
    }

    render() {
        if (this.state.resetComplete) {
            return <Redirect to={Route.toUrl("auth_login")} />
        }

        return (
            <div className="site-section bg-light">
                <Container>
                    <Row>
                        <Col lg={4} className="offset-lg-4">
                            <form onSubmit={this.formSubmitResetPassword} className="p-25 bg-white box-shadowed">
                                <h2 className="mb-4 text-black">Վերականգնել գաղտնաբառը</h2>

                                <FormGroup>
                                    <div className="input-area">
                                        <div className="input-content">
                                            <Label for="password">Նոր գաղտնաբառ <sup className="input-required fas fa-asterisk" /></Label>
                                            <Input type="password" id="password" className="rounded-0" onChange={this.validatePassword} />
                                        </div>
                                        <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                    <div className="input-area">
                                        <div className="input-content">
                                            <Label for="confirm">Հաստատել գաղտնաբառը <sup className="input-required fas fa-asterisk" /></Label>
                                            <Input type="password" id="confirm" className="rounded-0" onChange={this.validateConfirm} />
                                        </div>
                                        <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                    <Button color="success" className="w-100 py-2 px-4 rounded-0">Վերականգնել</Button>
                                    <ul className="auth-error-message" id="auth-error-message" />
                                    <ul className="auth-success-message" id="auth-success-message" />
                                </FormGroup>

                                <div className="loading-area" id="loading-area" />
                            </form>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

ResetPassword.propTypes = {
    dispatch: PropTypes.func.isRequired
};

export default connect((state) => {
    return {};
})(ResetPassword);
