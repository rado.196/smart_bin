import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Row, Col, FormGroup, Label, Input, Button } from "reactstrap";

import Route from "../../Routes";
import * as service from "../../Service/AuthService";

class ForgotPassword extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            codeSent: false,
        };

        this.validateEmailPhone = this.validateEmailPhone.bind(this);
        this.formSubmitReset = this.formSubmitReset.bind(this);
    }

    validateEmailPhone(event) {
        let element = event.target;
        if (!service.validEmailOrPhone(element)) {
            this.setState({
                emailOrPhoneData: element.value,
                emailOrPhoneValid: false,
            });
        } else {
            this.setState({
                emailOrPhoneData: element.value,
                emailOrPhoneValid: true,
            });
        }
    }

    formSubmitReset(event) {
        event.preventDefault();

        const invalid = !this.state.emailOrPhoneValid;
        if (invalid) {
            service.showAllErrors(event.target);
            return;
        }

        service.exec(this, event.target, service.forgotPassword, {
            email_or_phone: this.state.emailOrPhoneData,
        });
    }

    render() {
        if (this.state.codeSent) {
            return <Redirect to={Route.toUrl("auth_reset_verify")} />
        }

        if (this.props.isLogged) {
            return <Redirect to={Route.toUrl("user_history")} />
        }

        return (
            <div className="site-section bg-light">
                <Container>
                    <Row>
                        <Col lg={4} className="offset-lg-4">
                            <form onSubmit={this.formSubmitReset} className="p-25 bg-white box-shadowed">
                                <h2 className="mb-4 text-black">Վերականգնել գաղտնաբառը</h2>

                                <FormGroup>
                                    <div className="input-area">
                                        <div className="input-content">
                                            <Label for="email-phone">Էլ-փոստ / Բջջային համար <sup className="input-required fas fa-asterisk" /></Label>
                                            <Input type="text" id="email-phone" className="rounded-0" autoComplete="off" onChange={this.validateEmailPhone} />
                                        </div>
                                        <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                    <Button color="success" className="w-100 py-2 px-4 rounded-0">Վերականգնել</Button>
                                    <ul className="auth-error-message" id="auth-error-message" />
                                    <ul className="auth-success-message" id="auth-success-message" />
                                </FormGroup>

                                <div className="loading-area" id="loading-area" />
                            </form>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

ForgotPassword.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isLogged: PropTypes.bool.isRequired,
};

export default connect((state) => {
    return {
        isLogged: state.AuthReducer.isLogged,
    };
})(ForgotPassword);
