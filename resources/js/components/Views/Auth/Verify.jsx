import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Row, Col, FormGroup, Label, Input, Button } from "reactstrap";
import ReactCodeInput from "react-code-input";

import * as service from "../../Service/AuthService";
import Route from "../../Routes";

class Verify extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            verified: false,
            codeData: "",
            codeValid: false,
        };

        this.validateCode = this.validateCode.bind(this);
        this.formSubmitVerify = this.formSubmitVerify.bind(this);
    }

    validateCode(code) {
        if (code.length != 4) {
            this.setState({
                codeData: code,
                codeValid: false
            });
        } else {
            this.setState({
                codeData: code,
                codeValid: true
            });
        }
    }

    formSubmitVerify(event) {
        event.preventDefault();

        const invalid = !this.state.codeValid;
        if (invalid) {
            service.showAllErrors(event.target);
            return;
        }

        service.exec(this, event.target, service.verify, {
            code: this.state.codeData,
        });
    }

    render() {
        if (this.state.verified) {
            return <Redirect to={Route.toUrl("auth_login")} />
        }

        return (
            <div className="site-section bg-light">
                <Container>
                    <Row>
                        <Col lg={4} className="offset-lg-4">
                            <form onSubmit={this.formSubmitVerify} className="p-25 bg-white box-shadowed">
                                <h5 className="mb-4 text-black">Ակտիվացրեք Ձեր հաշիվը SmartBin կոդով։</h5>

                                <FormGroup>
                                    <div className="small bold italic text-label mT-50">
                                        SmartBin կոդը ուղարկվել է Ձեր {service.userProvider.is("email") ? "էլ-փոստ" : "բջջ․ համար"}ին։
                                    </div>
                                    <div className="input-area">
                                        <div className="input-content">
                                            <Label for="code">Ակտիվացման կոդ <sup className="input-required fas fa-asterisk" /></Label>
                                            <ReactCodeInput type="number" fields={4} onChange={this.validateCode} />
                                        </div>
                                        <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                    <Button color="success" className="w-100 py-2 px-4 rounded-0">Ակտիվացնել</Button>
                                    <ul className="auth-error-message" id="auth-error-message" />
                                    <ul className="auth-success-message" id="auth-success-message" />
                                </FormGroup>

                                <div className="loading-area" id="loading-area" />
                            </form>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

Verify.propTypes = {
    dispatch: PropTypes.func.isRequired
};

export default connect((state) => {
    return {};
})(Verify);
