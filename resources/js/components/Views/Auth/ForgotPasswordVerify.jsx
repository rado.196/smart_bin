import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Row, Col, FormGroup, Label, Input, Button } from "reactstrap";
import ReactCodeInput from "react-code-input";

import Route from "../../Routes";
import * as service from "../../Service/AuthService";

class ForgotPassword extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            validCode: false,
            codeData: "",
            codeValid: false,
        };

        this.validateCode = this.validateCode.bind(this);
        this.formSubmitVerify = this.formSubmitVerify.bind(this);
    }

    validateCode(code) {
        if (code.length != 4) {
            this.setState({
                codeData: code,
                codeValid: false
            });
        } else {
            this.setState({
                codeData: code,
                codeValid: true
            });
        }
    }

    formSubmitVerify(event) {
        event.preventDefault();

        const invalid = !this.state.codeValid;
        if (invalid) {
            service.showAllErrors(event.target);
            return;
        }

        service.exec(this, event.target, service.verifyReset, {
            code: this.state.codeData,
        });
    }

    render() {
        if (this.state.validCode) {
            return <Redirect to={Route.toUrl("auth_reset_password", { code: this.state.codeData })} />
        }

        if (this.props.isLogged) {
            return <Redirect to={Route.toUrl("user_history")} />
        }

        return (
            <div className="site-section bg-light">
                <Container>
                    <Row>
                        <Col lg={4} className="offset-lg-4">
                            <form onSubmit={this.formSubmitVerify} className="p-25 bg-white box-shadowed">
                                <h2 className="mb-4 text-black">Վերականգնել գաղտնաբառը</h2>

                                <FormGroup>
                                    <div className="small bold italic text-label mT-50">
                                        Վերականգնման կոդը ուղարկվել է Ձեր {service.userProvider.is("email") ? "էլ-փոստ" : "բջջ․ համար"}ին։
                                    </div>
                                    <div className="input-area">
                                        <div className="input-content">
                                            <Label for="code">Վերականգնման կոդ <sup className="input-required fas fa-asterisk" /></Label>
                                            <ReactCodeInput type="number" fields={4} onChange={this.validateCode} />
                                        </div>
                                        <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                    <Button color="success" className="w-100 py-2 px-4 rounded-0">Ակտիվացնել</Button>
                                    <ul className="auth-error-message" id="auth-error-message" />
                                    <ul className="auth-success-message" id="auth-success-message" />
                                </FormGroup>

                                <div className="loading-area" id="loading-area" />
                            </form>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

ForgotPassword.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isLogged: PropTypes.bool.isRequired,
};

export default connect((state) => {
    return {
        isLogged: state.AuthReducer.isLogged,
    };
})(ForgotPassword);
