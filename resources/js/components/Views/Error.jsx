import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class Error extends Component {
    render() {
        return (<Redirect to="/" />);
    }
}

Error.propTypes = {
    dispatch: PropTypes.func.isRequired
};

export default connect((state) => {
    return {};
})(Error);
