import React, { Component } from "react";
import AppLayout from "./App";

export default class User extends Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <AppLayout>
                <div className="user">
                    <div className="page-body">
                        {this.props.children}
                    </div>
                </div>
            </AppLayout>
        );
    }
}
