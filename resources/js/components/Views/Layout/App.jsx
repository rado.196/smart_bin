import React, { Component } from "react";
import { Redirect, Link, NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Row, Col, Navbar } from "reactstrap";

import Routes from "../../Routes";

class App extends Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div className="site-wrap">
                <div className="page-header">
                    <div className="site-navbar-wrap">
                        <div className="site-navbar-top bg-white">
                            <Container className="py-2">
                                <Row className="align-items-center">
                                    <Col xs={4} md={4} lg={3}>
                                        <Link to={Routes.toUrl("index")} className="d-flex align-items-center site-logo">SmartBin</Link>
                                    </Col>
                                    <Col xs={8} md={8} lg={9} className="top-right-icons">
                                        {this.props.isLogged ? (<ul className="unit-4 ml-auto text-right">
                                            <li>
                                                <NavLink exact={true} activeClassName="active" to={Routes.toUrl("user_history")} className="btn btn-success btn-outline-success rounded-0 py-2">
                                                    <i className="ti-layers-alt" />
                                                    <span>Պատմություն</span>
                                                </NavLink>
                                            </li>
                                            <li>
                                                <NavLink exact={true} activeClassName="active" to={Routes.toUrl("user_profile")} className="btn btn-success btn-outline-success rounded-0 py-2">
                                                    <i className="ti-user" />
                                                    <span>Անձնական էջ</span>
                                                </NavLink>
                                            </li>
                                            <li>
                                                <Link to={Routes.toUrl("auth_logout")} className="btn py-2">
                                                    <i className="ti-lock" />
                                                    <span>Դուրս գալ</span>
                                                </Link>
                                            </li>
                                        </ul>) : (<ul className="unit-4 ml-auto text-right">
                                            <li className="text-left">
                                                <NavLink exact={true} activeClassName="active" className="btn btn-success btn-outline-success rounded-0 text-white py-2" to={Routes.toUrl("auth_register")}>
                                                    <i className="ti-plus" />
                                                    <span>Գրանցվել</span>
                                                </NavLink>
                                            </li>
                                            <li>
                                                <NavLink exact={true} activeClassName="active" className="btn btn-success btn-outline-success rounded-0 text-white py-2" to={Routes.toUrl("auth_login")}>
                                                    <i className="ti-key" />
                                                    <span>Մուտք</span>
                                                </NavLink>
                                            </li>
                                        </ul>)}
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                        <div className="site-navbar bg-dark">
                            <Container className="py-2">
                                <Row className="align-items-center small-size-padding">
                                    <Col md={12}>
                                        <Navbar className="site-navigation text-left">
                                            <ul className="site-menu">
                                                <li>
                                                    <NavLink exact={true} activeClassName="active" to={Routes.toUrl("index")}>
                                                        <i className="ti-home" />
                                                        <span>Գլխավոր էջ</span>
                                                    </NavLink>
                                                </li>
                                                <li>
                                                    <NavLink exact={true} activeClassName="active" to={Routes.toUrl("front_about")}>
                                                        <i className="ti-thought" />
                                                        <span>Մեր մասին</span>
                                                    </NavLink>
                                                </li>
                                                <li>
                                                    <NavLink exact={true} activeClassName="active" to={Routes.toUrl("front_peoples_party")}>
                                                        <i className="ti-flag-alt" />
                                                        <span>Մարդամեջ</span>
                                                    </NavLink>
                                                </li>
                                                <li>
                                                    <NavLink exact={true} activeClassName="active" to={Routes.toUrl("front_contact")}>
                                                        <i className="ti-headphone-alt" />
                                                        <span>Հետադարձ կապ</span>
                                                    </NavLink>
                                                </li>
                                            </ul>
                                        </Navbar>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                    </div>
                </div>

                <div className="page-content bg-light">
                    {this.props.children}
                </div>

                <div className="page-footer bg-dark">
                    &copy; {window.CURRENT_YEAR} - Բոլոր իրավունքները պաշտպանված են։
                </div>
            </div>
        );
    }
}

App.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isLogged: PropTypes.bool.isRequired,
    user: PropTypes.object,
};

export default connect((state) => {
    return {
        isLogged: state.AuthReducer.isLogged,
        user: state.AuthReducer.user,
    };
})(App);
