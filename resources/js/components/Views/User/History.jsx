import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {Container, Row, Col, Card, CardBody, Table, Button, FormGroup, Form} from "reactstrap";
import axios from "axios";
import moment from "moment";
import { Chart } from "react-google-charts";

import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import { DateRangePicker } from "react-dates";

import Helper from "../../Helpers";

class History extends Component {
    constructor(props, context) {
        super(props, context);
        moment.locale('hy-am');

        this.state = {
            userRegisterDate: null,
            total: {
                // paper: 0,
                plastic: 0,
                glass: 0,
                loading: false,
            },
            filter: {
                // paper: 0,
                plastic: 0,
                glass: 0,
                startDate: null,
                endDate: null,
                focusedInput: null,
            },
            filtered: {
                items: [],
                loadedCount: 0,
                totalCount: 0,
                loading: false,
            }
        };

        this.loadLoggedUserRegisterDate = this.loadLoggedUserRegisterDate.bind(this);
        this.loadTotalCounts = this.loadTotalCounts.bind(this);
        this.filterTotalCounts = this.filterTotalCounts.bind(this);
        this.filterHistory = this.filterHistory.bind(this);
        this.filterFocusInput = this.filterFocusInput.bind(this);
        this.isOutsideRange = this.isOutsideRange.bind(this);
        this.renderMonthElement = this.renderMonthElement.bind(this);

        Helper.pageTitle("Պատմություն");
    }

    componentDidMount() {
        this.loadLoggedUserRegisterDate();
        this.loadTotalCounts();

        Helper.pageTitle("Պատմություն");
    }

    componentWillUpdate() {
        Helper.pageTitle("Պատմություն");
    }

    loadLoggedUserRegisterDate() {
        axios.post("/api/user-data/user-registered-date")
            .then((response) => {
                const userRegisterDate = new Date(response.data.date);

                this.setState({
                    userRegisterDate,
                }, () => {
                    const startDate = moment(userRegisterDate);
                    const endDate = moment();

                    this.filterTotalCounts({
                        startDate,
                        endDate,
                    });
                });
            });
    }

    loadTotalCounts() {
        const { total } = this.state;

        axios.post("/api/user-data/total")
            .then((response) => response.data)
            .then(({ /*countPaper, */countPlastic, countGlass }) => {
                total.loading = false;
                // total.paper = parseInt(countPaper);
                total.plastic = parseInt(countPlastic);
                total.glass = parseInt(countGlass);

                this.setState({
                    total,
                });
            });
    }

    filterTotalCounts({ startDate, endDate }) {
        const { filter, filtered } = this.state;

        filter.startDate = startDate;
        filter.endDate = endDate;

        filtered.items = [];
        filtered.loadedCount = 0;
        filtered.totalCount = 0;
        filtered.loading = false;

        this.setState({
            filter,
            filtered,
        });

        const postData = {
            startDate: null,
            endDate: null,
        };

        if (null != startDate) {
            postData.startDate = Helper.formatDate(startDate.toDate());
        }

        if (null != endDate) {
            postData.endDate = Helper.formatDate(endDate.toDate());
        }

        axios.post("/api/user-data/filter/count", postData)
            .then((response) => response.data)
            .then(({ /*countPaper, */countPlastic, countGlass, countHistory }) => {
                // filter.paper = parseInt(countPaper);
                filter.plastic = parseInt(countPlastic);
                filter.glass = parseInt(countGlass);
                filtered.totalCount = parseInt(countHistory);

                this.setState({
                    filter,
                });

                this.filterHistory();
            });
    }

    filterHistory() {
        const { filter, filtered } = this.state;
        if (filtered.totalCount === filtered.loadedCount) {
            return;
        }

        filtered.loading = true;
        this.setState({
            filtered,
        });

        const postData = {
            startDate: null,
            endDate: null,
            loaded: filtered.loadedCount,
        };

        if (null != filter.startDate) {
            postData.startDate = Helper.formatDate(filter.startDate.toDate());
        }

        if (null != filter.endDate) {
            postData.endDate = Helper.formatDate(filter.endDate.toDate());
        }

        axios.post("/api/user-data/filter/load", postData)
            .then((response) => response.data.list)
            .then((list) => {
                filtered.loadedCount += list.length;
                list.forEach((historyItem) => {
                    filtered.items.push(historyItem);
                });

                filtered.loading = false;
                this.setState({
                    filtered,
                });
            });
    }

    filterFocusInput(input) {
        const { filter } = this.state;
        filter.focusedInput = input;

        this.setState({
            filter,
        });
    }

    isOutsideRange(momentObj) {
        const now = new Date();
        const day = momentObj.toDate();
        const dob = this.state.userRegisterDate;

        return !(day >= dob && day <= now);
    }

    renderMonthElement({ month, onMonthSelect, onYearSelect }) {
        const dob = this.state.userRegisterDate;
        const now = new Date();

        const years = [];
        for (let i = dob.getFullYear(); i <= now.getFullYear(); ++i) {
            years.push(i);
        }

        return (
            <div className="calendar-month-year-select">
                <div>
                    <select value={month.month()} onChange={(e) => onMonthSelect(month, e.target.value)}>
                        {moment.months().map((label, value) => (
                            <option key={label} value={value} label={label} />
                        ))}
                    </select>
                </div>
                <div>
                    <select value={month.year()} onChange={(e) => onYearSelect(month, e.target.value)}>
                        {years.map((year) => (
                            <option key={year} value={year} label={year} />
                        ))}
                    </select>
                </div>
            </div>
        );
    }

    render() {
        const { total, filter, filtered } = this.state;
        const pieChart = {
            data: [
                ["Type", "Display name"],
                // ["Թուղթ", total.paper,],
                ["Պլաստիկ", total.plastic,],
                ["Ապակի", total.glass,],
            ],
            options: {
                is3D: true,
            }
        };

        return (
            <Container>
                <Row>
                    <Col lg={8} className="grid-margin d-flex flex-column">
                        <Row>
                            {/*<Col md={4} className="grid-margin">*/}
                                {/*<Card>*/}
                                    {/*<CardBody className="text-center">*/}
                                        {/*<div className="text-primary mb-4">*/}
                                            {/*<span className="ti-receipt icon-36px color-green" />*/}
                                            {/*<p className="font-weight-medium mt-2 color-green">Թուղթ</p>*/}
                                        {/*</div>*/}
                                        {/*<h1 className="font-weight-light">{Helper.formatNumber(total.paper)}</h1>*/}
                                    {/*</CardBody>*/}
                                {/*</Card>*/}
                            {/*</Col>*/}
                            <Col /*md={4}*/ md={6} className="grid-margin">
                                <Card>
                                    <CardBody className="text-center">
                                        <div className="text-primary mb-4">
                                            <span className="lnr lnr-coffee-cup icon-36px color-green" />
                                            <p className="font-weight-medium mt-2 color-green">Պլաստիկ</p>
                                        </div>
                                        <h1 className="font-weight-light">{Helper.formatNumber(total.plastic)}</h1>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col /*md={4}*/ md={6} className="grid-margin">
                                <Card>
                                    <CardBody className="text-center">
                                        <div className="text-primary mb-4">
                                            <span className="ti-light-bulb icon-36px color-green" />
                                            <p className="font-weight-medium mt-2 color-green">Ապակի</p>
                                        </div>
                                        <h1 className="font-weight-light">{Helper.formatNumber(total.glass)}</h1>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12} className="grid-margin">
                                <Card>
                                    <CardBody>
                                        <div className="history-filter">
                                            <DateRangePicker
                                                startDate={filter.startDate} startDateId="MyDatePickerStartDate"
                                                endDate={filter.endDate} endDateId="MyDatePickerEndDate"
                                                onDatesChange={this.filterTotalCounts}
                                                onFocusChange={this.filterFocusInput}
                                                focusedInput={filter.focusedInput}
                                                startDatePlaceholderText="Սկսած"
                                                endDatePlaceholderText="Վերջացրած"
                                                isOutsideRange={this.isOutsideRange}
                                                hideKeyboardShortcutsPanel={true}
                                                numberOfMonths={1}
                                                renderMonthElement={this.renderMonthElement}
                                            />
                                        </div>
                                        <h4 className="card-title mB-50">Պատմություն ({filtered.totalCount})</h4>
                                        <FormGroup>
                                            <div className="table-responsive">
                                                <Table>
                                                    <thead>
                                                        <tr>
                                                            <th className="bdwT-0">
                                                                <span className="history-table-th-icon">
                                                                    <i className="ti-calendar" title="Օր / Ժամ" />
                                                                </span>
                                                                <span className="history-table-th-text">
                                                                    Օր / Ժամ
                                                                </span>
                                                            </th>
                                                            {/*<th className="bdwT-0">*/}
                                                                {/*<span className="history-table-th-icon">*/}
                                                                    {/*<i className="ti-receipt" title="Թուղթ" />*/}
                                                                {/*</span>*/}
                                                                {/*<span className="history-table-th-text">*/}
                                                                    {/*Թուղթ*/}
                                                                {/*</span>*/}
                                                            {/*</th>*/}
                                                            <th className="bdwT-0">
                                                                <span className="history-table-th-icon">
                                                                    <i className="lnr lnr-coffee-cup" title="Պլաստիկ" />
                                                                </span>
                                                                <span className="history-table-th-text">
                                                                    Պլաստիկ
                                                                </span>
                                                            </th>
                                                            <th className="bdwT-0">
                                                                <span className="history-table-th-icon">
                                                                    <i className="ti-light-bulb" title="Ապակի" />
                                                                </span>
                                                                <span className="history-table-th-text">
                                                                    Ապակի
                                                                </span>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {!!(filtered.items.length != 0) && (filtered.items.map((h, i) => (
                                                            <tr key={i}>
                                                                <td title={Helper.formatDateTime(h.created_at)}>{Helper.humanizeDateDifference(h.created_at)}</td>
                                                                {/*<td>{Helper.formatNumber(h.paper)}</td>*/}
                                                                <td>{Helper.formatNumber(h.plastic)}</td>
                                                                <td>{Helper.formatNumber(h.glass)}</td>
                                                            </tr>
                                                        )))}
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            {filtered.loading && <td /*colSpan={4}*/ colSpan={3} className="text-center">
                                                                <span className="table-loading-icon" />
                                                            </td>}

                                                            {!filtered.loading && filtered.loadedCount !== filtered.totalCount && <td colSpan={4} className="text-center">
                                                                <Button onClick={this.filterHistory} size="sm" color="success">
                                                                    <span className="lnr lnr-plus" />
                                                                    Ավելին
                                                                </Button>
                                                            </td>}

                                                            {!filtered.loading && filtered.totalCount === 0 && <td colSpan={4}>
                                                                Պատմություն չկա
                                                            </td>}
                                                        </tr>
                                                    </tfoot>
                                                </Table>
                                            </div>
                                        </FormGroup>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={4} className="grid-margin stretch-card">
                        <Card>
                            <CardBody>
                                <FormGroup>
                                    <div className="d-flex justify-content-between align-items-start">
                                        <h4 className="card-title">Ընդհանուր</h4>
                                    </div>
                                </FormGroup>
                                <FormGroup className="mB-70">
                                    {total.loading ? (
                                        <span className="table-loading-icon" />
                                    ) : (
                                        (/*total.paper || */total.plastic || total.glass) ? (
                                            <Chart chartType="PieChart" {...pieChart}
                                                   width="100%"
                                                   loader={<div>Loading ...</div>}
                                            />
                                    ): (<span>
                                            <i>Պատմությունը դատարկ է։</i>
                                        </span>)
                                    )}
                                </FormGroup>
                            </CardBody>
                            <CardBody className="card-body-with-bg">

                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        );
    }
}

History.propTypes = {
    dispatch: PropTypes.func.isRequired
};

export default connect((state) => {
    return {};
})(History);
