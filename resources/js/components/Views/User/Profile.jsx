import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Row, Col, Card, CardBody, Label, Input, Button, FormGroup } from "reactstrap";
import InputMask from "react-input-mask";
import reactTriggerChange from "react-trigger-change";
import ReactCodeInput from "react-code-input";

import * as service from "../../Service/AuthService";
import Helper from "../../Helpers";

class Profile extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            firstNameData: props.user.first_name,
            firstNameValid: true,
            lastNameData: props.user.last_name,
            lastNameValid: true,
            addressData: props.user.address,
            addressValid: true,
            phoneData: props.user.phone,
            phoneValid: true,
            emailData: props.user.email,
            emailValid: true,

            oldPasswordData: "",
            oldPasswordValid: false,
            newPasswordData: "",
            newPasswordValid: false,
            confirmData: "",
            confirmValid: false,
        };

        this.validateFirstName = this.validateFirstName.bind(this);
        this.validateLastName = this.validateLastName.bind(this);
        this.validateAddress = this.validateAddress.bind(this);
        this.validatePhone = this.validatePhone.bind(this);
        this.validateEmail = this.validateEmail.bind(this);
        this.formSubmitProfile = this.formSubmitProfile.bind(this);

        this.validateOldPassword = this.validateOldPassword.bind(this);
        this.validateNewPassword = this.validateNewPassword.bind(this);
        this.validatePasswordConfirm = this.validatePasswordConfirm.bind(this);
        this.formSubmitPassword = this.formSubmitPassword.bind(this);

        Helper.pageTitle("Անձնական էջ");
    }

    componentDidMount() {
        Helper.pageTitle("Անձնական էջ");
    }

    componentWillUpdate() {
        Helper.pageTitle("Անձնական էջ");
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            firstNameData: nextProps.user.first_name,
            lastNameData: nextProps.user.last_name,
            addressData: nextProps.user.address,
            phoneData: nextProps.user.phone,
            emailData: nextProps.user.email,
        });
    }

    validateFirstName(event) {
        let element = event.target;
        if (!service.validFirstName(element)) {
            this.setState({
                firstNameData: element.value,
                firstNameValid: false
            });
        } else {
            this.setState({
                firstNameData: element.value,
                firstNameValid: true
            });
        }
    }

    validateLastName(event) {
        let element = event.target;
        if (!service.validLastName(element)) {
            this.setState({
                lastNameData: element.value,
                lastNameValid: false
            });
        } else {
            this.setState({
                lastNameData: element.value,
                lastNameValid: true
            });
        }
    }

    validateAddress(event) {
        let element = event.target;
        if (!service.validAddress(element)) {
            this.setState({
                addressData: element.value,
                addressValid: false
            });
        } else {
            this.setState({
                addressData: element.value,
                addressValid: "" != element.value
            });
        }
    }

    validatePhone(event) {
        let element = event.target;
        if (!service.validPhone(element)) {
            this.setState({
                phoneData: element.value,
                phoneValid: false
            });
        } else {
            this.setState({
                phoneData: element.value,
                phoneValid: "" != element.value
            });
        }
    }

    validateEmail(event) {
        let element = event.target;
        if (!service.validEmail(element)) {
            this.setState({
                emailData: element.value,
                emailValid: false
            });
        } else {
            this.setState({
                emailData: element.value,
                emailValid: "" != element.value
            });
        }
    }

    formSubmitProfile(event) {
        event.preventDefault();

        const invalid = !this.state.firstNameValid ||
            !this.state.lastNameValid ||
            !this.state.addressValid ||
            !this.state.phoneValid ||
            !this.state.emailValid;

        if (invalid) {
            service.showAllErrors(event.target);
            return;
        }

        service.exec(this, event.target, service.update, {
            first_name: this.state.firstNameData,
            last_name: this.state.lastNameData,
            address: this.state.addressData,
            phone: this.state.phoneData,
            email: this.state.emailData,
        });
    }

    validateOldPassword(event) {
        let element = event.target;
        if (!service.validPassword(element)) {
            this.setState({
                oldPasswordData: element.value,
                oldPasswordValid: false,
            });
        } else {
            this.setState({
                oldPasswordData: element.value,
                oldPasswordValid: "" != element.value,
            });
        }
    }

    validateNewPassword(event) {
        let element = event.target;
        if (!service.validPassword(element)) {
            this.setState({
                newPasswordData: element.value,
                newPasswordValid: false,
            });
        } else {
            this.setState({
                newPasswordData: element.value,
                newPasswordValid: "" != element.value,
            });
        }

        let confirm = document.querySelector("#confirm-password");
        reactTriggerChange(confirm);
    }

    validatePasswordConfirm(event) {
        let element = event.target;
        if (!service.validConfirm(element, this.state.newPasswordData)) {
            this.setState({
                confirmData: element.value,
                confirmValid: false,
            });
        } else {
            this.setState({
                confirmData: element.value,
                confirmValid: true,
            });
        }
    }

    formSubmitPassword(event) {
        event.preventDefault();

        const invalid = !this.state.oldPasswordValid ||
            !this.state.newPasswordValid ||
            !this.state.confirmValid;

        if (invalid) {
            service.showAllErrors(event.target);
            return;
        }

        service.exec(this, event.target, service.changePassword, {
            old_password: this.state.oldPasswordData,
            new_password: this.state.newPasswordData,
        }).then(() => {
            this.setState({
                oldPasswordData: "",
                oldPasswordValid: false,
                newPasswordData: "",
                newPasswordValid: false,
                confirmData: "",
                confirmValid: false,
            });
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col lg={4} className="offset-lg-2 mB-20">
                        <Card>
                            <CardBody>
                                <h3 className="font-weight-light mB-50">Անձնական տվյալներ</h3>

                                <form onSubmit={this.formSubmitProfile}>
                                    <FormGroup>
                                        <div className="input-area">
                                            <div className="input-content">
                                                <Label for="first-name">Անուն <sup className="input-required fas fa-asterisk" /></Label>
                                                <Input type="text" id="first-name" className="rounded-0" autoComplete="off" value={this.state.firstNameData} onChange={this.validateFirstName} />
                                            </div>
                                            <span className="error-message">
                                                <span className="error-content" />
                                            </span>
                                        </div>
                                    </FormGroup>

                                    <FormGroup>
                                        <div className="input-area">
                                            <div className="input-content">
                                                <Label for="last-name">Ազգանուն <sup className="input-required fas fa-asterisk" /></Label>
                                                <Input type="text" id="last-name" className="rounded-0" autoComplete="off" value={this.state.lastNameData} onChange={this.validateLastName} />
                                            </div>
                                            <span className="error-message">
                                                <span className="error-content" />
                                            </span>
                                        </div>
                                    </FormGroup>

                                    <FormGroup>
                                        <div className="input-area">
                                            <div className="input-content">
                                                <Label for="address">Բնակության հասցե <sup className="input-required fas fa-asterisk" /></Label>
                                                <Input id="address" className="rounded-0" autoComplete="off" value={this.state.addressData} onChange={this.validateAddress} />
                                            </div>
                                            <span className="error-message">
                                                <span className="error-content" />
                                            </span>
                                        </div>
                                    </FormGroup>

                                    <FormGroup>
                                        <div className="input-area">
                                            <div className="input-content">
                                                <Label for="phone">Բջջային համար <sup className="input-required fas fa-asterisk" /></Label>
                                                <InputMask mask="+37499999999" value={this.state.phoneData} onChange={this.validatePhone}>{(inputProps) => (
                                                    <Input type="tel" id="phone" className="rounded-0 monospaced" autoComplete="off" {...inputProps} />
                                                )}</InputMask>
                                            </div>
                                            <span className="error-message">
                                                <span className="error-content" />
                                            </span>
                                        </div>
                                    </FormGroup>

                                    <FormGroup>
                                        <div className="input-area">
                                            <div className="input-content">
                                                <Label for="email">Էլ-փոստ</Label>
                                                <Input type="email" id="email" className="rounded-0" autoComplete="off" value={this.state.emailData} onChange={this.validateEmail} />
                                            </div>
                                            <span className="error-message">
                                                <span className="error-content" />
                                            </span>
                                        </div>
                                    </FormGroup>

                                    <FormGroup>
                                        <div className="input-area">
                                            <div className="input-content">
                                                <Label for="last-name">Կոդ</Label>
                                                <ReactCodeInput type="text" fields={4} value={this.props.user.code.toString()} disabled />
                                            </div>
                                            <span className="error-message">
                                                <span className="error-content" />
                                            </span>
                                        </div>
                                    </FormGroup>

                                    <FormGroup>
                                        <Button color="success" className="w-100 py-2 px-4 rounded-0">Փոխել տվյալները</Button>
                                        <ul className="auth-error-message" id="auth-error-message" />
                                        <ul className="auth-success-message" id="auth-success-message" />
                                    </FormGroup>

                                    <div className="loading-area" id="loading-area" />
                                </form>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg={4}>
                        <Card>
                            <CardBody>
                                <h3 className="font-weight-light mB-50">Գաղտնաբառ</h3>

                                <form onSubmit={this.formSubmitPassword}>
                                    <FormGroup>
                                        <div className="input-area">
                                            <div className="input-content">
                                                <Label for="old-password">Հին գաղտնաբառ <sup className="input-required fas fa-asterisk" /></Label>
                                                <Input type="password" id="old-password" className="rounded-0" autoComplete="off" value={this.state.oldPasswordData} onChange={this.validateOldPassword} />
                                            </div>
                                            <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                        </div>
                                    </FormGroup>

                                    <FormGroup>
                                        <div className="input-area">
                                            <div className="input-content">
                                                <Label for="new-password">Նոր գաղտնաբառ <sup className="input-required fas fa-asterisk" /></Label>
                                                <Input type="password" id="new-password" className="rounded-0" autoComplete="off" value={this.state.newPasswordData} onChange={this.validateNewPassword} />
                                            </div>
                                            <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                        </div>
                                    </FormGroup>

                                    <FormGroup>
                                        <div className="input-area">
                                            <div className="input-content">
                                                <Label for="confirm-password">Հաստատել նոր գաղտնաբառը <sup className="input-required fas fa-asterisk" /></Label>
                                                <Input type="password" id="confirm-password" className="rounded-0" autoComplete="off" value={this.state.confirmData} onChange={this.validatePasswordConfirm} />
                                            </div>
                                            <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                        </div>
                                    </FormGroup>

                                    <FormGroup>
                                        <Button color="success" className="w-100 py-2 px-4 rounded-0">Փոխել գաղտնաբառը</Button>
                                        <ul className="auth-error-message" id="auth-error-message" />
                                        <ul className="auth-success-message" id="auth-success-message" />
                                    </FormGroup>

                                    <div className="loading-area" id="loading-area" />
                                </form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        );
    }
}

Profile.propTypes = {
    dispatch: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
};

export default connect((state) => {
    return {
        user: state.AuthReducer.user,
    };
})(Profile);
