import PAuthRegister from "./Views/Auth/Register";
import PAuthVerify from "./Views/Auth/Verify";
import PAuthLogin from "./Views/Auth/Login";
import PAuthLogout from "./Views/Auth/Logout";
import PAuthForgotPassword from "./Views/Auth/ForgotPassword";
import PAuthForgotPasswordVerify from "./Views/Auth/ForgotPasswordVerify";
import PAuthResetPassword from "./Views/Auth/ResetPassword";
import PUserHistory from "./Views/User/History";
import PUserProfile from "./Views/User/Profile";
import PHome from "./Views/Front/Home";
import PAbout from "./Views/Front/About";
import PPeoplesParty from "./Views/Front/PeoplesParty";
import PContact from "./Views/Front/Contact";
import PError from "./Views/Error";

import LApp from "./Views/Layout/App";
import LUser from "./Views/Layout/User";

import Route from "./Routing/Router";

Route.group("/auth", () => {
    Route.add("auth_register", {
        path: "/register",
        component: PAuthRegister,
        layout: LApp,
    });

    Route.add("auth_verify", {
        path: "/verify",
        component: PAuthVerify,
        layout: LApp,
    });

    Route.add("auth_login", {
        path: "/login",
        component: PAuthLogin,
        layout: LApp,
    });

    Route.add("auth_logout", {
        path: "/logout",
        component: PAuthLogout,
        layout: LApp,
    });

    Route.add("auth_forgot_password", {
        path: "/forgot-password",
        component: PAuthForgotPassword,
        layout: LApp,
    });

    Route.add("auth_reset_verify", {
        path: "/forgot-password/verify",
        component: PAuthForgotPasswordVerify,
        layout: LApp,
    });

    Route.add("auth_reset_password", {
        path: "/reset/:code",
        component: PAuthResetPassword,
        layout: LApp,
    });
});

Route.add("front_about", {
    path: "/about-us",
    component: PAbout,
    layout: LApp,
});

Route.add("front_peoples_party", {
    path: "/peoples-party",
    component: PPeoplesParty,
    layout: LApp,
});

Route.add("front_contact", {
    path: "/contact",
    component: PContact,
    layout: LApp,
});

Route.group("/user", () => {
    Route.add("user_history", {
        path: "/history",
        component: PUserHistory,
        layout: LUser,
        auth: true,
    });

    Route.add("user_profile", {
        path: "/profile",
        component: PUserProfile,
        layout: LUser,
        auth: true,
    });

    Route.add("user_change_password", {
        path: "/change-password",
        component: PUserHistory,
        layout: LUser,
        auth: true,
    });
});

Route.index(PHome, LApp);
Route.error(PError, LApp);

export default Route;
