import axios from "axios";
import * as actions from "../actions";

const initialState = {
    isLogged: false,
    user: null,
};

const getValue = (key, payloadUser, oldUser) => {
    let value = "";
    if (payloadUser.hasOwnProperty(key)) {
        value = payloadUser[key];
    } else {
        value = oldUser[key];
    }

    return !value ? "" : value;
};

const check = (state) => {
    if (!!localStorage.getItem("access_token")) {
        axios.defaults.headers.common["Authorization"] = `Bearer ${localStorage.getItem("access_token")}`;
        
        let user = localStorage.getItem("logged_user");
        user = JSON.parse(user);
        
        state = Object.assign({}, state, {
            isLogged: true,
            user,
        });
    } else {
        state = Object.assign({}, state, {
            isLogged: false,
            user: null,
        });
    }

    return state;
};

const login = (state, payload) => {
    const jwtToken = payload.access_token;

    localStorage.setItem("access_token", jwtToken);
    axios.defaults.headers.common["Authorization"] = `Bearer ${jwtToken}`;

    const user = {
        id: getValue("id", payload.user),
        first_name: getValue("first_name", payload.user),
        last_name: getValue("last_name", payload.user),
        address: getValue("address", payload.user),
        phone: getValue("phone", payload.user),
        email: getValue("email", payload.user),
        code: getValue("code", payload.user),
    };

    localStorage.setItem("logged_user", JSON.stringify(user));
    state = Object.assign({}, state, {
        isLogged: true,
        user,
    });

    return state;
};

const logout = (state) => {
    localStorage.removeItem("access_token");
    localStorage.removeItem("logged_user");

    delete axios.defaults.headers.common["Authorization"];
    state = Object.assign({}, state, {
        isLogged: false,
        user: null,
    });

    return state;
};

const update = (state, payload) => {
    const user = JSON.parse(localStorage.getItem("logged_user"));

    user.id = getValue("id", payload.user, user);
    user.first_name = getValue("first_name", payload.user, user);
    user.last_name = getValue("last_name", payload.user, user);
    user.address = getValue("address", payload.user, user);
    user.phone = getValue("phone", payload.user, user);
    user.email = getValue("email", payload.user, user);
    user.code = getValue("code", payload.user, user);

    localStorage.setItem("logged_user", JSON.stringify(user));
    state = Object.assign({}, state, {
        isLogged: true,
        user,
    });

    return state;
};

export default (state = initialState, { type, payload = null }) => {
    switch (type) {
        case actions.AUTH_CHECK:
            return check(state);

        case actions.AUTH_LOGIN:
            return login(state, payload);

        case actions.AUTH_LOGOUT:
            return logout(state);

        case actions.AUTH_UPDATE:
            return update(state, payload);
    }

    return state;
};
