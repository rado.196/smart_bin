import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import AllReducers from "./Reducers";

const Store = createStore(AllReducers, applyMiddleware(thunk));
export default Store;
