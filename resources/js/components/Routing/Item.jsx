import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import Router from "../Routing/Router";

const makeRouteView = (args, ViewComponent) => {
    const PageLayout = args.layout;
    return <Route {...args} render={(props) => (
        <PageLayout>
            <ViewComponent {...props} />
        </PageLayout>
    )} />;
};

const makeRouteRedirect = (args) => {
    const redirectData = {
        pathname: Router.toUrl("auth_login"),
        state: {
            from: args.location,
        }
    };

    return <Route {...args} render={(props) => (
        <Redirect to={redirectData} />
    )} />;
};

const Item = ({ component, isLogged, ...args }) => {
    if (!args.auth || args.auth && isLogged) {
        return makeRouteView(args, component);
    } else {
        return makeRouteRedirect(args);
    }
};

export default connect((state) => {
    return {
        isLogged : state.AuthReducer.isLogged,
    }
})(Item);
