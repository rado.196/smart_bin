<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Գաղտնաբառերը պետք է լինեն առնվազն վեց նիշ եւ համապատասխանում հաստատմանը:',
    'reset' => 'Ձեր գաղտնաբառը վերականգնվել է:',
    'sent' => 'Մենք էլփոստով ենք ուղարկել ձեր գաղտնաբառը վերականգման հղումը:',
    'token' => 'Այս գաղտնաբառի վերակայման նշանն անվավեր է:',
    'user' => 'Մենք չենք կարող գտնել այն էլեկտրոնային փոստի հասցեով օգտվող:',

];
