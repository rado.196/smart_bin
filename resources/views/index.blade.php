<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="theme-color" content="#1da556" />

    <title>{{ env('APP_NAME', 'SmartBin') }}</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
</head>
<body>
    {{-- JavaScript not supported --}}
    <noscript id="noscript">
        <h1 class="text-center mT-160 p-40">
            You need to enable JavaScript to run {{ env('APP_NAME', 'SmartBin') }} app.
        </h1>
    </noscript>

    {{-- Run React.JS app --}}
    <div id="app-root"></div>

    <script type="text/javascript">window.CURRENT_YEAR={{ date('Y') }}</script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>