<?php

namespace App\Listeners;

use App\Events\ResetPasswordEvent;
use App\Traits\NotifyBySmsOrEmailTrait;

class ResetPasswordListener
{
    use NotifyBySmsOrEmailTrait;

    public function handle(ResetPasswordEvent $event)
    {
        $passwordReset = $event->getPasswordReset();
        $user = $passwordReset->user;
        $message = 'Ձեր գաղտնաբառի վերականգնման կոդը՝ ' . $passwordReset->code;

        $this->eventSend($event->getPrior(), $user, $message);
    }
}
