<?php

namespace App\Events;

use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class ResetPasswordEvent
{
    use SerializesModels;

    private $passwordReset;
    private $prior;

    public function __construct(PasswordReset $passwordReset, $providerValue)
    {
        $this->passwordReset = $passwordReset;
        $this->prior = filter_var($providerValue, FILTER_VALIDATE_EMAIL)
            ? User::PROVIDER_EMAIL
            : User::PROVIDER_PHONE;
    }

    public function getPasswordReset()
    {
        return $this->passwordReset;
    }

    public function getPrior()
    {
        return $this->prior;
    }
}
