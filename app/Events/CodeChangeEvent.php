<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class CodeChangeEvent
{
    use SerializesModels;

    private $user;
    private $prior;

    public function __construct(User $user, $providerValue)
    {
        $this->user = $user;
        $this->prior = filter_var($providerValue, FILTER_VALIDATE_EMAIL)
            ? User::PROVIDER_EMAIL
            : User::PROVIDER_PHONE;
    }

    public function getUnverifiedUser()
    {
        return $this->user;
    }

    public function getPrior()
    {
        return $this->prior;
    }
}
