<?php

namespace App\Http\Middleware;

use App\Traits\AuthApiLogoutTrait;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VerifyJwtToken
{
    use AuthApiLogoutTrait;

    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        $needToLogout =
            Auth::check() && is_null($token) ||
            Auth::guest() && !is_null($token);

        return $this->verify($request, $next, $needToLogout);
    }

    private function verify(Request $request, Closure $next, $needToLogout)
    {
        if ($needToLogout) {
            $user = Auth::user();
            $this->forgetUser($user);
        }

        $response = $next($request);
        if ($needToLogout) {
            $json = $response->getData();
            $json->auth_remove_token = true;

            $response = new JsonResponse($json);
        }

        return $response;
    }
}
