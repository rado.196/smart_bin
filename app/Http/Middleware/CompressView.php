<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CompressView
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $isProduction = in_array(env('APP_ENV'), ['prod', 'production']);
        $isDebugging = env('APP_DEBUG');

        if ($isProduction && !$isDebugging && $this->isViewResponse($response)) {
            $buffer = $response->getContent();
            $replaces = [];
            
            if(strpos($buffer,'<pre>') !== false) {
                $replaces = self::$replacesWithPre;
            } else {
                $replaces = self::$replacesWithoutPre;
            }
            
            $buffer = preg_replace(
                array_keys($replaces),
                array_values($replaces),
                $buffer
            );
            
            $response->setContent($buffer);
            ini_set('zlib.output_compression', 'On');
        }

        return $response;
    }

    private function isViewResponse($response)
    {
        $isResponse = is_object($response) && $response instanceof Response;
        $isTextHtml = strtolower(strtok($response->headers->get('Content-Type'), ';')) === 'text/html';

        return $isResponse && $isTextHtml;
    }

    private static $replacesWithPre = [
        '/<!--[^\[](.*?)[^\]]-->/s' => '',
        "/<\?php/" => '<?php ',
        "/\r/" => '',
        "/>\n</" => '><',
        "/>\s+\n</" => '><',
        "/>\n\s+</" => '><',
    ];
    
    private static $replacesWithoutPre = [
        '/<!--[^\[](.*?)[^\]]-->/s' => '',
        "/<\?php/" => '<?php ',
        "/\n([\S])/" => '$1',
        "/\r/" => '',
        "/\n/" => '',
        "/\t/" => '',
        "/ +/" => ' ',
    ];
}
