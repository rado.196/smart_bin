<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;

class InsertController extends Controller
{
    private static $API_TOKEN_LENGTH = 4;

    public function generateToken(Request $request)
    {
        $user = User::where('code', $request->post('code'))->first();
        if (is_null($user)) {
            return response()->json([
                'status' => 'failed',
                'message' => 'invalid_code::code_not_assigned',
            ]);
        }

        Transaction::where('user_id', $user->id)
            ->where('state', Transaction::STATE_WAITING)
            ->update([
                'state' => Transaction::STATE_CANCELED,
            ]);

        $hash = Hash::make($request->post('code'));
        $hash = crc32($hash);
        $hash = base_convert($hash, 10, 16);

        $user->last_token = substr($hash, 0, self::$API_TOKEN_LENGTH);
        $user->save();

        Transaction::create([
            'user_id' => $user->id,
            'token' => $user->last_token,
            'state' => Transaction::STATE_WAITING,
        ]);

        return response()->json([
            'status' => 'success',
            'token' => $user->last_token,
        ]);
    }

    public function storeData(Request $request)
    {
        $user = User::where('code', $request->post('code'))->first();
        if (is_null($user)) {
            return response()->json([
                'status' => 'failed',
                'message' => 'invalid_code::code_not_assigned_to_user',
            ]);
        }

        $verificationErrorMessage = $this->verifyToken($request, $user);
        if (false !== $verificationErrorMessage) {
            return response()->json([
                'status' => 'failed',
                'message' => $verificationErrorMessage,
            ]);
        }

        return $this->insertData($request, $user);
    }

    private function verifyToken(Request $request, User& $user)
    {
        $tokenSession = $user->last_token;
        $tokenRequest = $request->post('token');

        if ($tokenSession != $tokenRequest) {
            return 'invalid_token::session_changed';
        }

        return false;
    }

    private function insertData(Request $request, User& $user)
    {
        $history = History::create([
            'user_id' => $user->id,
            'ip_address' => $request->ip(),
//            'paper' => $request->post('paper'),
            'plastic' => $request->post('plastic'),
            'glass' => $request->post('glass'),
        ]);

        $transaction = Transaction::where('user_id', $user->id)
            ->where('state', Transaction::STATE_WAITING)
            ->first();

        $transaction->state = Transaction::STATE_COMPLETE;
        $transaction->history_id = $history->id;
        $transaction->save();

        $user->last_token = null;
        $user->save();

        return response()->json([
            'status' => 'success',
        ]);
    }
}
