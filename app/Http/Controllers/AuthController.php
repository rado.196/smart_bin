<?php

namespace App\Http\Controllers;

use App\Events\CodeChangeEvent;
use App\Events\ResetPasswordEvent;
use App\Events\UserRegisterEvent;
use App\Models\PasswordReset;
use App\Models\User;
use App\Rules\EmailRule;
use App\Rules\PhoneRule;
use App\Traits\AuthApiLogoutTrait;
use App\Traits\DigitalCodeTrait;
use App\Traits\JwtAuthenticateTrait;
use App\Traits\NotifyBySmsOrEmailTrait;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use AuthApiLogoutTrait;
    use NotifyBySmsOrEmailTrait;
    use DigitalCodeTrait;
    use JwtAuthenticateTrait;

    public function register(Request $request)
    {
        $validateData = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'address' => 'required|string',
            'phone' => new PhoneRule(),
            'password' => 'required|string|min:6',
        ];

        if (!is_null($request->post('email'))) {
            $validateData['email'] = new EmailRule();
        }

        $request->validate($validateData);
        $provider = filter_var($request->post('email'), FILTER_VALIDATE_EMAIL)
            ? User::PROVIDER_EMAIL
            : User::PROVIDER_PHONE;

        $user = User::create([
            'first_name' => $request->post('first_name'),
            'last_name' => $request->post('last_name'),
            'address' => $request->post('address'),
            'phone' => $request->post('phone'),
            'email' => $request->post('email'),
            'code' => $this->getNextFreeCode(),
            'password' => Hash::make($request->post('password')),
            'provider' => $provider,
        ]);

        event(new UserRegisterEvent($user));
        return response()->json([
            'status' => 'success',
            'provider' => $this->getUserProvider($user),
        ], 201);
    }

    public function verify(Request $request)
    {
        $request->validate([
            'code' => 'required|string|min:4|max:4',
        ]);

        $code = $request->post('code');
        $user = User::where('code', $code)->first();

        if (!is_null($user)) {
            if (!$user->verified) {
                $user->verified = true;
                $user->save();

                $responseData = $this->jwtAuthenticate($user);
                $responseData['status'] = 'success';

                return response()->json($responseData);
            }

            return response()->json([
                'message' => 'Օգտատերոջ հաշիվը արդեն ակտիվ է։',
            ]);
        }

        return response()->json([
            'message' => 'Օգտատերը չի գտնվել։',
        ]);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email_or_phone' => 'required|string',
            'password' => 'required|string|min:6',
        ]);

        $emailPhone = $request->post('email_or_phone');
        $user = User::where('deleted_at', null)
            ->where(function ($query) use ($emailPhone) {
                $query->where('email', $emailPhone)
                    ->orWhere('phone', $emailPhone)
                    ->orWhere('phone', '+374' . substr($emailPhone, 1))
                    ->orWhere('phone', '+374' . $emailPhone);
            })
            ->first();

        if (is_null($user) || !Hash::check($request->post('password'), $user->password)) {
            return response()->json([
                'message' => 'Սխալ էլ-փոստ/բջջ համար կամ գաղտնաբառ։',
            ], 401);
        } elseif (!$user->verified) {
            $user->code = $this->changeCode($user->code);
            $user->save();

            event(new CodeChangeEvent($user, $emailPhone));
            return response()->json([
                'status' => 'not_verified',
                'provider' => $this->getUserProvider($user),
            ]);
        }

        $responseData = $this->jwtAuthenticate($user);
        $responseData['status'] = 'success';

        return response()->json($responseData);
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $this->forgetUser($user);
    }

    public function loggedUser()
    {
        $user = Auth::user();
        $logged = !is_null($user);
        if (!$logged) {
            $this->forgetUser($user);
            $user = null;
        }

        return response()->json([
            'auth_logged_user' => $user,
        ]);
    }

    public function forgot(Request $request)
    {
        $request->validate([
            'email_or_phone' => 'required|string',
        ]);

        $emailPhone = $request->post('email_or_phone');
        $user = User::where('deleted_at', null)
            ->where(function ($query) use ($emailPhone) {
                $query->where('email', $emailPhone)
                    ->orWhere('phone', $emailPhone)
                    ->orWhere('phone', '+374' . substr($emailPhone, 1))
                    ->orWhere('phone', '+374' . $emailPhone);
            })
            ->first();

        if (is_null($user)) {
            return response()->json([
                'message' => 'Սխալ էլ-փոստ/բջջ համար։',
            ]);
        }

        PasswordReset::where('user_id', $user->id)->delete();
        $passwordReset = PasswordReset::create([
            'user_id' => $user->id,
            'code' => $this->generateRandomCode(),
        ]);

        event(new ResetPasswordEvent($passwordReset, $emailPhone));
        return response()->json([
            'status' => 'success',
            'provider' => $this->getUserProvider($user),
        ]);
    }

    public function forgotVerify(Request $request)
    {
        $request->validate([
            'code' => 'required|string|min:4|max:4',
        ]);

        $code = $request->post('code');
        $passwordReset = PasswordReset::where('code', $code)->first();

        if (!is_null($passwordReset)) {
            return response()->json([
                'status' => 'success',
            ]);
        }

        return response()->json([
            'message' => 'Սխալ կոդ։',
        ], 401);
    }

    public function reset(Request $request)
    {
        $request->validate([
            'password' => 'required|string',
            'code' => 'required',
        ]);

        $passwordReset = PasswordReset::where('code', $request->post('code'))
            ->first();

        if (is_null($passwordReset)) {
            return response()->json([
                'message' => 'Սխալ կոդ։',
            ]);
        }

        $user = $passwordReset->user;
        if (is_null($user)) {
            return response()->json([
                'message' => 'auth_error_reset_email'
            ]);
        }

        $user->password = Hash::make($request->post('password'));
        $user->save();

        $passwordReset->delete();
        return response()->json([
            'status' => 'success',
        ]);
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        $validateData = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'address' => 'required|string',
        ];

        if (!is_null($request->post('email'))) {
            if ($user->email != $request->post('email')) {
                $validateData['email'] = new EmailRule();
            }
        }

        if ($user->phone != $request->post('phone')) {
            $validateData['email'] = new PhoneRule();
        }

        $request->validate($validateData);

        $user->first_name = $request->post('first_name');
        $user->last_name = $request->post('last_name');
        $user->address = $request->post('address');
        $user->phone = $request->post('phone');
        $user->email = $request->post('email');
        $user->save();

        return response()->json([
            'status' => 'success',
        ]);
    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        if (!Hash::check($request->post('old_password'), $user->password)) {
            return response()->json([
                'message' => 'Հին գաղտնաբառը սխալ է։',
            ]);
        }

        $user->password = Hash::make($request->post('new_password'));
        $user->save();

        return response()->json([
            'status' => 'success',
        ]);
    }
}
