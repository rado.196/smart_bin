<?php

namespace App\Http\Controllers;

use App\Models\History;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    public function registeredDate(Request $request)
    {
        $date = Auth::user()->created_at->format('m/d/Y');
        return response()->json([
            'date' => $date,
        ]);
    }

    public function total(Request $request)
    {
//        $paperCount = History::where('user_id', Auth::id())->sum('paper');
        $plasticCount = History::where('user_id', Auth::id())->sum('plastic');
        $glassCount = History::where('user_id', Auth::id())->sum('glass');

        return response()->json([
//            'countPaper' => $paperCount,
            'countPlastic' => $plasticCount,
            'countGlass' => $glassCount,
        ]);
    }

    public function topThree(Request $request)
    {
//        $raw = DB::raw('sum(`paper` + `plastic` + `glass`) as `total`');
        $raw = DB::raw('sum(`plastic` + `glass`) as `total`');

        $topThrees = History::query()
            ->select($raw)
            ->groupBy('user_id')
            ->limit(3)
            ->pluck('total')
            ->toArray();

        foreach ($topThrees as $i => $topThree) {
            $topThrees[$i] = intval($topThree);
        }

        rsort($topThrees);

        $maxCount = array_fill(0, 3, 0);
        foreach ($topThrees as $i => $topThree) {
            $maxCount[$i] = $topThree;
        }

        return response()->json([
            'topThree' => $maxCount,
        ]);
    }

    public function filterCount(Request $request)
    {
        [$sDate, $eDate] = $this->getFilterDates($request);

        $historyCount = History::whereBetween('created_at', [$sDate, $eDate])
            ->where('user_id', Auth::id())
            ->count();

//        $paperCount = History::whereBetween('created_at, [$sDate, $eDate])
//            ->where('user_id', Auth::id())
//            ->sum('paper');

        $plasticCount = History::whereBetween('created_at', [$sDate, $eDate])
            ->where('user_id', Auth::id())
            ->sum('plastic');

        $glassCount = History::whereBetween('created_at', [$sDate, $eDate])
            ->where('user_id', Auth::id())
            ->sum('glass');

        return response()->json([
            'countHistory' => $historyCount,
//            'countPaper' => $paperCount,
            'countPlastic' => $plasticCount,
            'countGlass' => $glassCount,
        ]);
    }

    public function filterLoad(Request $request)
    {
        [$sDate, $eDate] = $this->getFilterDates($request);

        $history = History::whereBetween('created_at', [$sDate, $eDate])
            ->where('user_id', Auth::id())
            ->orderBy('id', 'desc')
            ->skip($request->post('loaded'))
            ->take(10)
            ->get();

        return response()->json([
            'list' => $history,
        ]);
    }

    private function getFilterDates(Request $request)
    {
        $sDate = $request->post('startDate');
        if (is_null($sDate)) {
            $sDate = Auth::user()->created_at->format('m/d/Y');
        }

        $eDate = $request->post('endDate');
        if (is_null($eDate)) {
            $eDate = date('m/d/Y');
        }

        $sDate = date('Y-m-d 00:00:00', strtotime($sDate));
        $eDate = date('Y-m-d 23:59:59', strtotime($eDate));

        return [$sDate, $eDate];
    }
}

