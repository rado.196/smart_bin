<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;
    use SoftDeletes;

    const PROVIDER_EMAIL = 'user_provider_email';
    const PROVIDER_PHONE = 'user_provider_phone';

    protected $fillable = [
        'first_name',
        'last_name',
        'address',
        'phone',
        'email',
        'code',
        'verified',
        'password',
        'provider',
        'last_token',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function history()
    {
        return $this->hasMany(History::class, 'user_id', 'id');
    }
}
