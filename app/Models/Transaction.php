<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    const STATE_WAITING = 'waiting';
    const STATE_COMPLETE = 'complete';
    const STATE_CANCELED = 'canceled';

    protected $table = 'transactions';

    protected $fillable = [
        'user_id',
        'history_id',
        'token',
        'state',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function history()
    {
        return $this->hasOne(User::class, 'id', 'history_id');
    }
}
