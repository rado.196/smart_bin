<?php

namespace App\Traits;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait JwtAuthenticateTrait
{
    private final function jwtAuthenticate(User& $user)
    {
        Auth::guard('api')->setUser($user, true);

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();
        $expiration = Carbon::parse($tokenResult->token->expires_at);

        return [
            'access_token' => $tokenResult->accessToken,
            'expires_at' => $expiration->toDateTimeString(),
            'user' => [
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'address' => $user->address,
                'phone' => $user->phone,
                'email' => $user->email,
                'code' => $user->code,
            ],
        ];
    }
}
