<?php

namespace App\Traits;

use App\Models\User;

trait AuthApiLogoutTrait
{
    private function forgetUser(User $user = null)
    {
        if (is_null($user)) {
            return;
        }

        $user->token()->revoke();
        $user->save();
    }
}
