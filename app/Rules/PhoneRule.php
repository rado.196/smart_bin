<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class PhoneRule implements Rule
{
    private $isValid = true;
    private $isFree = true;

    public function passes($attribute, $value)
    {
        $this->checkIsValid($value);
        $this->checkIsFree($value);

        return $this->isValid && $this->isFree;
    }

    public function message()
    {
        if (!$this->isValid) {
            return 'Անվավեր բջջային հեռախոսահամար:';
        }

        if (!$this->isFree) {
            return 'Հեռախոսահամարը արդեն զբաղված է:';
        }
    }

    private function checkIsValid($value)
    {
        $this->isValid = preg_match('/^\+374(41|43|44|55|77|91|93|94|95|96|98|99)[\d]{6}$/', $value);
    }

    private function checkIsFree($value)
    {
        $this->isFree = !User::where('phone', $value)->exists();
    }
}
