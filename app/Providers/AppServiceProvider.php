<?php

namespace App\Providers;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param UrlGenerator $urlGenerator
     *
     * @return void
     */
    public function boot(UrlGenerator $urlGenerator)
    {
        Schema::defaultStringLength(191);

        if (self::isHttps()) {
            $urlGenerator->formatScheme('https');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (self::isHttps()) {
            $this->app['request']->server->set('HTTPS', true);
        }
    }

    private static function isHttps()
    {
        $header = Request::server('HTTP_X_FORWARDED_PROTO');
        return strtolower($header) == 'https';
    }
}
