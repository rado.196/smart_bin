<?php

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// // migrate app
// Route::get('/app/fresh-migrate', function () {
//     ini_set('max_execution_time', 300);
//
//     Artisan::call('migrate:fresh');
//     Artisan::call('db:seed');
//     Artisan::call('passport:install', [
//         '--force' => true,
//     ]);
//
//     return 'Application migrated successfully';
// });

// clear app cache
Route::get('/app/clear-cache', function () {
    ini_set('max_execution_time', 300);

    Artisan::call('cache:clear');
    Artisan::call('config:clear');

    return redirect('/');
});

// forward to react app
Route::get('/{url}', function () {
    return view('index');
})->where('url', '^((?!api).)*$');
