echo -e '\n'
echo -e '*****************************************************************'
echo -e '** Changing folder mode to 777 (recursive)                      **'
#chmod -R 777 ./

echo -e '\n'
echo -e '*****************************************************************'
echo -e '** Migrating database tables                                   **'
php artisan migrate

echo -e '\n'
echo -e '*****************************************************************'
echo -e '** Seeding database data                                       **'
php artisan db:seed

echo -e '\n'
echo -e '*****************************************************************'
echo -e '** Installing passport keys                                    **'
php artisan passport:install --force
